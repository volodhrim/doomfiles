# doom-emacs-private

My private config for [doom-emacs](https://github.com/hlissner/doom-emacs).
Shamelessly stolen from [hlissner, aka the author of doom-emacs](https://github.com/hlissner/doom-emacs-private).

## Install

``` sh
git clone https://gitlab.com/volodhrim/doomfiles ~/.config/doom
```
